<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */

namespace code2magic\payment\WayForPay;

use code2magic\payment\AbstractCallbackData;
use code2magic\payment\Status;
use WayForPay\SDK\Domain\TransactionService;
use yii\helpers\ArrayHelper;

/**
 * Class CallbackData
 *
 * @method TransactionService getRaw()
 *
 * @package code2magic\payment\WayForPay
 */
class CallbackData extends AbstractCallbackData
{
    /**
     * CallbackData constructor.
     * @param TransactionService $transactionService
     */
    public function __construct(TransactionService $transactionService)
    {
        $this->_data = $transactionService;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->get('orderReference');
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
        switch ($this->get('status')) {
            // STATUS_PENDING
            case TransactionService::STATUS_CREATED:
            case TransactionService::STATUS_PENDING:
            case TransactionService::STATUS_IN_PROCESSING:
            case TransactionService::STATUS_WAIT_AUTH_COMPLETE:
                return new Status(Status::PENDING);
                break;
            // STATUS_SUCCESS
            case TransactionService::STATUS_APPROVED:
                return new Status(Status::SUCCESS);
                break;
            // STATUS_REFUNDED
            case TransactionService::STATUS_REFUNDED:
            case TransactionService::STATUS_VOIDED:
                return new Status(Status::REFUNDED);
                break;
            // STATUS_DECLINED
            //case TransactionService::STATUS_DECLINED:
            //case TransactionService::STATUS_REFUND_IN_PROCESSING:
            default:
                return new Status(Status::DECLINED);
        }
    }
}
