<?php

namespace code2magic\payment\WayForPay;

use code2magic\payment\ApiInterface;
use code2magic\payment\CallbackDataInterface;
use WayForPay\SDK\Domain\TransactionService;
use WayForPay\SDK\Exception\ApiException;
use WayForPay\SDK\Handler\ServiceUrlHandler;
use yii\base\Component;
use yii\helpers\Json;
use yii\web\Request;
use yii\web\Response;

/**
 * Class Api
 * @package code2magic\payment\WayForPay
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class Api extends Component implements ApiInterface
{
    /**
     * @var ServiceUrlHandler
     */
    protected $_serviceUrlHandler;

    /**
     * Api constructor.
     * @param ServiceUrlHandler $handler
     * @param array $config
     */
    public function __construct(ServiceUrlHandler $handler, $config = [])
    {
        parent::__construct($config);
        $this->_serviceUrlHandler = $handler;
    }

    /**
     * @param Request $request
     * @return CallbackData
     * @throws \yii\base\InvalidConfigException
     * @throws \Exception
     */
    public function getCallbackData(Request $request): ?CallbackDataInterface
    {
        $rawContentType = $request->getContentType();
        if (($pos = strpos($rawContentType, ';')) !== false) {
            // e.g. text/html; charset=UTF-8
            $contentType = substr($rawContentType, 0, $pos);
        } else {
            $contentType = $rawContentType;
        }
        if (in_array($contentType, ['application/json', 'multipart/form-data',], true)) {
            $data = $request->getBodyParams();
        } else {
            $data = Json::decode($request->getRawBody());
        }
        try {
            $transaction = $this->_serviceUrlHandler
                ->parseRequestFromArray((array)$data)
                ->getTransaction();
        } catch (ApiException $exception) {
            $transaction = TransactionService::fromArray($data);
        }
        return new CallbackData($transaction);
    }

    /**
     * @param CallbackDataInterface $callback_data
     * @return Response|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getResponse(CallbackDataInterface $callback_data): ?Response
    {
        $response = \Yii::createObject([
            'class' => \yii\web\Response::class,
            'format' => \yii\web\Response::FORMAT_RAW,
            'data' => $this->_serviceUrlHandler->getSuccessResponse($callback_data->getRaw()),
        ]);
        $response->getHeaders()->set('Content-Type', 'application/json; charset=UTF-8');
        return $response;
    }
}
