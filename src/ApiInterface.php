<?php

namespace code2magic\payment;

use yii\web\Request;
use yii\web\Response;

/**
 * Interface PaymentApiInterface
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
interface ApiInterface
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function getCallbackData(Request $request): ?CallbackDataInterface;

    /**
     * @param $callback_data
     * @return Response|null
     */
    public function getResponse(CallbackDataInterface $callback_data): ?Response;
}
