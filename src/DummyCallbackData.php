<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */

namespace code2magic\payment;

use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;

/**
 * Class DummyCallbackData
 *
 * @package code2magic\payment
 */
class DummyCallbackData extends AbstractCallbackData
{
    /**
     * CallbackData constructor.
     *
     * @param array $callbackData
     */
    public function __construct(array $callbackData)
    {
        if(!($order_id = ArrayHelper::getValue($callbackData, ['order_id',]))){
            throw new InvalidConfigException('Elemante "order_id" is required!');
        }
        if($status = ArrayHelper::getValue($callbackData, ['status',])) {
            if(!($status instanceof Status)){
                $status = new Status($status);
            }
        } else {
            throw new InvalidConfigException('Elemante "status" is required!');
        }
        $this->_data = [
            'order_id' => $order_id,
            'status' => $status,
        ];
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->get('order_id');
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
       return $this->get('status');
    }
}
