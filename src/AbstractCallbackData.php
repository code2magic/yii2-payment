<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */

namespace code2magic\payment;

use yii\helpers\ArrayHelper;

/**
 * Class AbstractCallbackData
 * @package code2magic\payment
 */
abstract class AbstractCallbackData implements CallbackDataInterface
{
    /**
     * @var mixed
     */
    protected $_data;

    /**
     * Get attribute value from raw data
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function get($key, $default = null){
        $getter = 'get' . $key;
        if (method_exists($this->_data, $getter)) {
            return $this->_data->$getter();
        }
        return ArrayHelper::getValue($this->_data, $key, $default);
    }

    /**
     * Get raw data
     * @return mixed
     */
    public function getRaw(){
        return $this->_data;
    }

    /**
     * @inheritDoc
     */
    abstract public function getOrderId();

    /**
     * @inheritDoc
     */
    abstract public function getStatus(): Status;
}
