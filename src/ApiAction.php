<?php

namespace code2magic\payment;

/**
 * Class PaymentApiAction
 * @package code2magic\payment
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class ApiAction extends \yii\base\Action
{
    /**
     * @var ApiInterface
     */
    public $api;

    /**
     * @var callable
     */
    public $process_request;

    /**
     * @return mixed
     * @throws \RuntimeException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\di\NotInstantiableException
     */
    public function run()
    {
        $callback_data = \Yii::$container->invoke([$this->api, 'getCallbackData',], []);
        if (!($callback_data instanceof CallbackDataInterface)) {
            throw new \RuntimeException('Invalid callback_data!');
        }
        if (is_callable($this->process_request)) {
            \Yii::$container->invoke($this->process_request, [$callback_data,]);
        }
        return \Yii::$container->invoke([$this->api, 'getResponse',], [$callback_data,]);
    }
}
