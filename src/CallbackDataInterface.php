<?php

namespace code2magic\payment;

/**
 * Interface CallbackDataInterface
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
interface CallbackDataInterface
{
    /**
     * Get order id
     * @return mixed
     */
    public function getOrderId();

    /**
     * Get order status
     * @return Status
     */
    public function getStatus(): Status;
}
