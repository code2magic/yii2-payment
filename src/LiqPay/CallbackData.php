<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */

namespace code2magic\payment\LiqPay;

use code2magic\payment\AbstractCallbackData;
use code2magic\payment\Status;
use WayForPay\SDK\Domain\TransactionService;

/**
 * Class CallbackData
 *
 * @method array getRaw()
 *
 * @package code2magic\payment\WayForPay
 */
class CallbackData extends AbstractCallbackData
{
    /**
     * CallbackData constructor.
     * @param array $transactionService
     */
    public function __construct(array $transactionService)
    {
        $this->_data = $transactionService;
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->get('order_id');
    }

    /**
     * @return Status
     */
    public function getStatus(): Status
    {
        // todo 'subscribed', 'unsubscribed'
        switch ($this->get('status')) {
            // STATUS_PENDING
            case '3ds_verify':
            case 'captcha_verify':
            case 'cvv_verify':
            case 'ivr_verify':
            case 'otp_verify':
            case 'password_verify':
            case 'phone_verify':
            case 'pin_verify':
            case 'receiver_verify':
            case 'sender_verify':
            case 'senderapp_verify':
            case 'wait_qr':
            case 'wait_sender':
            case 'cash_wait':
            case 'hold_wait':
            case 'invoice_wait':
            case 'prepared':
            case 'processing':
            case 'wait_accept':
            case 'wait_card':
            case 'wait_compensation':
            case 'wait_lc':
            case 'wait_reserve':
            case 'wait_secure':
                return new Status(Status::PENDING);
                break;
            // STATUS_SUCCESS
            case 'success':
                return new Status(Status::SUCCESS);
                break;
            // STATUS_REFUNDED
            case 'reversed':
                return new Status(Status::REFUNDED);
                break;
            // STATUS_DECLINED
            //case 'error':
            //case 'failure':
            default:
                return new Status(Status::DECLINED);
        }
    }
}
