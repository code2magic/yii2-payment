<?php

namespace code2magic\payment\LiqPay;

use code2magic\LiqPay\LiqPay;
use code2magic\payment\ApiInterface;
use code2magic\payment\CallbackDataInterface;
use yii\base\Component;
use yii\web\Request;
use yii\web\Response;

/**
 * Class Api
 * @package code2magic\payment\LiqPay
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */
class Api extends Component implements ApiInterface
{
    /**
     * @var LiqPay
     */
    private $api;

    /**
     * Api constructor.
     * @param LiqPay $api
     * @param array $config
     */
    public function __construct(LiqPay $api, $config = [])
    {
        parent::__construct($config);
        $this->api = $api;
    }

    /**
     * @param Request $request
     * @return CallbackData
     */
    public function getCallbackData(Request $request): ?CallbackDataInterface
    {
        $data = $request->getBodyParam('data');
        if ($this->api->validateData($data, $request->getBodyParam('signature'))) {
            return new CallbackData($this->api->decode_params($data));
        }
        throw new \RuntimeException('Request signature mismatch');
    }

    /**
     * @param CallbackDataInterface $callback_data
     * @return Response|null
     * @throws \yii\base\InvalidConfigException
     */
    public function getResponse(CallbackDataInterface $callback_data): ?Response
    {
        return \Yii::createObject([
            'class' => \yii\web\Response::class,
            'format' => \yii\web\Response::FORMAT_RAW,
            'data' => '',
        ]);
    }
}
