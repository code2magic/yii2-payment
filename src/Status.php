<?php
/**
 * @author Roman Hlushchak <spell6inder@gmail.com>
 */

namespace code2magic\payment;

use yii2mod\enum\helpers\BaseEnum;

/**
 * Class Status
 * @package code2magic\payment
 */
class Status extends BaseEnum
{
    public const PENDING = 'pending';
    public const SUCCESS = 'success';
    public const REFUNDED = 'refunded';
    public const DECLINED = 'declined';

    /**
     * @var string message category
     * You can set your own message category for translate the values in the $list property
     * Values in the $list property will be automatically translated in the function `listData()`
     */
    public static $messageCategory = 'code2magic/payment/status';

    /**
     * @var array
     */
    public static $list = [
        self::PENDING => 'Pending',
        self::SUCCESS => 'Success',
        self::REFUNDED => 'Refunded',
        self::DECLINED => 'Declined',
    ];
}
