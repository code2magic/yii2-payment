Yii2 Payment
==========

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run
```bash
php composer.phar require code2magic/yii2-payment --prefer-dist
```
or add
```
"code2magic/yii2-payment": "*"
```

to the `require` section of your `composer.json` file.

## Usage
